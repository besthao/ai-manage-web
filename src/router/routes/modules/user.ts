import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const USER: AppRouteRecordRaw = {
  path: '/user',
  name: 'user',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '板块秀',
    icon: 'icon-user',
    requiresAuth: true,
    hideInMenu: true,
    order: 6,
  },
  children: [
    {
      path: 'info',
      name: 'Info',
      component: () => import('@/views/user/info/index.vue'),
      meta: {
        locale: '板块秀',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    // {
    //   path: 'setting',
    //   name: 'Setting',
    //   component: () => import('@/views/user/setting/index.vue'),
    //   meta: {
    //     locale: 'menu.user.setting',
    //     requiresAuth: true,
    //     roles: ['*'],
    //   },
    // },
  ],
};

export default USER;
