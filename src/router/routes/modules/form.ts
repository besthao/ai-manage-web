import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const FORM: AppRouteRecordRaw = {
  path: '/form',
  name: 'form',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '左侧视野',
    icon: 'icon-settings',
    requiresAuth: true,
    hideInMenu: true,
    order: 3,
  },
  children: [
    {
      path: 'step',
      name: 'Step',
      component: () => import('@/views/form/step/index.vue'),
      meta: {
        locale: '左侧视野',
        requiresAuth: true,
        roles: ['admin'],
      },
    },
  ],
};

export default FORM;
