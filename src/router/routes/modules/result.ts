import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const RESULT: AppRouteRecordRaw = {
  path: '/result',
  name: 'result',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '指数晴雨表',
    icon: 'icon-check-circle',
    requiresAuth: true,
    order: 7,
  },
  children: [
    {
      path: 'success',
      name: 'Success',
      component: () => import('@/views/result/success/index.vue'),
      meta: {
        locale: '指数晴雨表',
        requiresAuth: true,
        roles: ['admin'],
      },
    }
  ],
};

export default RESULT;
